using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RetargeterIllumination : MonoBehaviour {

	public enum RetargeterState
	{
		ON,
		OFF
	}

	public GameObject[] goRetargeterModels;

	[Range(0,2)]
	public float glowStrength;
	private float currentGlowStrength;

	private RetargeterState retargeterState;

	// Use this for initialization
	void Start () {
		this.retargeterState = RetargeterState.OFF;
		this.currentGlowStrength = this.glowStrength;
	}	

	[ContextMenu("Activate")]
	public void Activate()
	{
		this.retargeterState = RetargeterState.ON;
	}

	[ContextMenu("DeActivate")]
	public void DeActivate()
	{
		this.retargeterState = RetargeterState.OFF;
	}


	void SetIlluminations()
	{
		Color glowColor = new Color(
			this.currentGlowStrength,								this.currentGlowStrength,								this.currentGlowStrength,
			1
		);

		foreach(GameObject go in this.goRetargeterModels)
		{
	go.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.SetColor("_EmissionColor", glowColor);
		}
	}

	
	// Update is called once per frame
	void Update () {
		SetIlluminations();

		if(this.retargeterState == RetargeterState.OFF)
		{
			if(this.currentGlowStrength > 0)
			{
				this.currentGlowStrength -= Time.deltaTime;
			}
			else
			{
				return;
			}
		}
		else 
		{
			//State is ON
			if(this.currentGlowStrength < this.glowStrength)
			{
				this.currentGlowStrength += Time.deltaTime;
			}
			else if(this.currentGlowStrength > this.glowStrength)
			{
				this.currentGlowStrength = this.glowStrength;
			}
		}
	}
}
