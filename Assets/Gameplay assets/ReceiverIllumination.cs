using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceiverIllumination : MonoBehaviour {

	public enum ReceiverState
	{
		ON,
		OFF
	}
	
	public GameObject goWindowLigth;
	public GameObject goBaseNode;
	public GameObject goParticle;

	[Range(0,1)]
	public float glowStrength;
	private float currentGlowStrength;

	private ReceiverState receiverState;

	// Use this for initialization
	void Start () {
		this.receiverState = ReceiverState.OFF;
		this.currentGlowStrength = 1;
	}

	[ContextMenu("Activate")]
	public void Activate()
	{
		this.receiverState = ReceiverState.ON;
	}

	[ContextMenu("DeActivate")]
	public void DeActivate()
	{
		this.receiverState = ReceiverState.OFF;
	}
	
	void SetIlluminations()
	{
		Color glowColor = new Color(
			this.currentGlowStrength,								this.currentGlowStrength,								this.currentGlowStrength,
			1
		);
		this.goWindowLigth.GetComponent<Renderer>().material.SetColor("_EmissionColor", glowColor);
		this.goBaseNode.GetComponent<Renderer>().material.SetColor("_EmissionColor", glowColor);
		this.goParticle.GetComponent<Renderer>().material.SetColor("_TintColor", glowColor);

		//Change position of particle
		this.goParticle.transform.localPosition = new Vector3(
			this.goParticle.transform.localPosition.x,
			0.7f * this.currentGlowStrength,
			this.goParticle.transform.localPosition.x
		);

		
		//Change scale of particle
		//this.goParticle.transform.localScale = new Vector3(
		//	this.goParticle.transform.localScale.x,
		//	this.currentGlowStrength,
		//	this.goParticle.transform.localScale.x
		//);
	}

	// Update is called once per frame
	void Update () {
		SetIlluminations();

		if(this.receiverState == ReceiverState.OFF)
		{
			if(this.currentGlowStrength > 0)
			{
				this.currentGlowStrength -= Time.deltaTime;
			}
			else
			{
				return;
			}
		}
		else 
		{
			//State is ON
			if(this.currentGlowStrength < this.glowStrength)
			{
				this.currentGlowStrength += Time.deltaTime;
			}
			else if(this.currentGlowStrength > this.glowStrength)
			{
				this.currentGlowStrength = this.glowStrength;
			}
		}
	}
}
