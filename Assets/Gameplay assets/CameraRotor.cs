﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotor : MonoBehaviour {

    public RetargeterIllumination[] retargeters;
    public ReceiverIllumination[] receivers;
    public float rotationSpeed = 1;

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			this.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);
		}
		if (Input.GetKey(KeyCode.RightArrow))
		{
			this.transform.Rotate(-Vector3.up * Time.deltaTime * rotationSpeed);
		}

	}

    public void ActivateAll()
    {
        foreach(RetargeterIllumination rti in this.retargeters)
        {
            rti.Activate();
        }
        foreach(ReceiverIllumination rci in this.receivers)
        {
            rci.Activate();
        }
    }
    public void DeActivateAll()
    {
        foreach (RetargeterIllumination rti in this.retargeters)
        {
            rti.DeActivate();
        }
        foreach (ReceiverIllumination rci in this.receivers)
        {
            rci.DeActivate();
        }
    }
}
