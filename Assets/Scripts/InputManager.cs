﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

struct TouchStruct
{
    public TouchStruct(int touchIndex, Vector2 touchPosition, float startTime)
    {
        this.touchIndex = touchIndex;
        this.touchPosition = touchPosition;
        this.startTime = startTime;
    }

    private int touchIndex;
    public int TouchIndex
    {
        get
        {
            return touchIndex;
        }
        set
        {
            touchIndex = value;
        }
    }

    private Vector2 touchPosition;
    public Vector2 TouchPosition
    {
        get
        {
            return touchPosition;
        }
        set
        {
            touchPosition = value;
        }
    }

    private float startTime;
    public float StartTime
    {
        get
        {
            return startTime;
        }
        set
        {
            startTime = value;
        }
    }
}

public class InputManager : MonoBehaviour {

    public static InputManager instance;

    private PlayerMovement myPlayerMovement;
    private List<TouchStruct> activeTouches;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        myPlayerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        activeTouches = new List<TouchStruct>();
        GameManager.instance.playerRespawned += UpdatePlayer;
    }
	
    void UpdatePlayer()
    {
        myPlayerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update () {
        if (myPlayerMovement != null)
        {
            if (Input.GetKeyDown(KeyCode.A))
                myPlayerMovement.Movement(2);
            else if (Input.GetKeyDown(KeyCode.D))
                myPlayerMovement.Movement(1);
            else if (Input.GetKeyDown(KeyCode.W))
                myPlayerMovement.Movement(3);
            else if (Input.GetKeyDown(KeyCode.S))
                myPlayerMovement.Movement(0);

            foreach (Touch touch in Input.touches)
            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        activeTouches.Add(new TouchStruct(touch.fingerId, touch.position, Time.time));
                        break;
                    case TouchPhase.Ended:
                        TouchStruct touchStart = activeTouches.Single(t => t.TouchIndex == touch.fingerId);

                        if(Vector3.Distance(touch.position, touchStart.TouchPosition) > 50)
                            if ((touch.position - touchStart.TouchPosition).x < 0 && (touch.position - touchStart.TouchPosition).y > 0)
                                myPlayerMovement.Movement(2);
                            else if ((touch.position - touchStart.TouchPosition).x > 0 && (touch.position - touchStart.TouchPosition).y < 0)
                                myPlayerMovement.Movement(1);
                            else if ((touch.position - touchStart.TouchPosition).x > 0 && (touch.position - touchStart.TouchPosition).y > 0)
                                myPlayerMovement.Movement(3);
                            else if ((touch.position - touchStart.TouchPosition).x < 0 && (touch.position - touchStart.TouchPosition).y < 0)
                                myPlayerMovement.Movement(0);

                        activeTouches.Remove(touchStart);
                        break;
                }
            }
        }
    }

    public void SetMyPlayerMovement(PlayerMovement playerMovement)
    {
        myPlayerMovement = playerMovement;
    }
}
