﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using MovementEffects;
using System.Collections.Generic;

public class FadeManager : MonoBehaviour {

    public static FadeManager instance;

    private float duration = 1.5f;
    private Image fadeImage;
    private Text fadeLevelNumber;
    private GameObject myGameObject;

	void Awake () {
        instance = this;

        myGameObject = gameObject;

        if (transform.childCount > 1)
        {
            fadeLevelNumber = transform.GetChild(1).GetComponent<Text>();
            fadeLevelNumber.text = SceneManager.GetActiveScene().buildIndex.ToString();
            Timing.RunCoroutine(FadeLevelNumber());
        }

        Timing.RunCoroutine(FadeStartScene());
    }

    public IEnumerator<float> FadeLevelNumber()
    {
        float timer = 0f;
        float myDuration = duration * (2f / 3);

        while (timer < myDuration)
        {
            if(fadeLevelNumber != null)
                fadeLevelNumber.canvasRenderer.SetAlpha(((myDuration / 2) - Mathf.Abs(myDuration / 2 - timer)) / (myDuration / 2) * 0.5f);
            timer += Time.deltaTime;
            yield return Timing.WaitForOneFrame;
        }

        if (fadeLevelNumber != null)
            fadeLevelNumber.gameObject.SetActive(false);
    }

    public IEnumerator<float> FadeStartScene()
    {
        fadeImage = transform.GetChild(0).GetComponent<Image>();
        if (SceneManager.GetActiveScene().buildIndex % 5 == 0 && SceneManager.GetActiveScene().buildIndex != 0)
            fadeImage.color = new Color(0.25f, 0, 0, 1);

       fadeImage.CrossFadeAlpha(0, duration, false);

        yield return Timing.WaitForSeconds(duration);

        if(myGameObject != null)
            myGameObject.SetActive(false);
    }

    public IEnumerator<float> FadeEndScene()
    {
        myGameObject.SetActive(true);

        fadeImage.canvasRenderer.SetAlpha(0);
        fadeImage.color = new Color(0, 0, 0, 1);
        fadeImage.CrossFadeAlpha(1, duration, false);

        yield return Timing.WaitForSeconds(2);

        SceneNavigation.instance.ChangeLevel(SceneManager.GetActiveScene().buildIndex);
    }
}
