﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class FinishButtonManager : MonoBehaviour {

    private GameObject[] receptors;
    private bool finished;
    private AudioSource myAudio;
    private ParticleSystem myParticle;

    void Start()
    {
        receptors = GameObject.FindGameObjectsWithTag("Receptor");
        myAudio = GetComponent<AudioSource>();
        myParticle = GetComponentInChildren<ParticleSystem>();
        myParticle.gameObject.SetActive(false);
    }

    void OnTriggerEnter(Collider otherCollider)
    {
        if (!finished)
        {
            if (otherCollider.tag == "Player" || otherCollider.tag == "Deflector")
            {
                myAudio.Play();
                myParticle.gameObject.SetActive(true);
            }

            CheckFinished(otherCollider);
        }
    }

    void OnTriggerStay(Collider otherCollider)
    {
        if (!finished)
            CheckFinished(otherCollider);
    }

    private void OnTriggerExit(Collider otherCollider)
    {
        if (otherCollider.tag == "Player" || otherCollider.tag == "Deflector")
            myParticle.gameObject.SetActive(false);
    }

    /// <summary>
    /// Checks if there is a player or deflector on the button and checks if the receptor is filled.
    /// </summary>
    /// <param name="otherCollider"></param>
    private void CheckFinished(Collider otherCollider)
    {
        if (otherCollider.tag == "Player" || otherCollider.tag == "Deflector")
        {
            finished = true;

            //If the player is hitting the button, the receptors are checked.
            foreach (GameObject gameObject in receptors)
                if (gameObject.GetComponent<ReceptorManager>().chargedValue < 100)
                    finished = false;

            if (finished)
            {
                LevelCompletion.instance.LevelEnd();
                GameManager.LevelFinished = true;
            }
        }
    }
}
