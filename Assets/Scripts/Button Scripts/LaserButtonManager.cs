﻿using UnityEngine;
using System.Collections;

public class LaserButtonManager : MonoBehaviour {

    public LaserManager laserManager;
    private AudioSource myAudio;


    void Start()
    {
        myAudio = GetComponent<AudioSource>();    
    }


void OnTriggerEnter(Collider otherCollider)
    {
        //If the player is hitting the button, the laser moves.
        if ((otherCollider.CompareTag("Player") || otherCollider.CompareTag("Deflector")) && !GameManager.LevelFinished)
        {
            myAudio.Play();
            laserManager.moveLaserFromButton();
        }
    }
}
