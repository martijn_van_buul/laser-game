﻿using UnityEngine;
using System.Collections;

public class TeleporterManager : MonoBehaviour {

    private GameObject otherTeleporter;

	void Start () {
	    foreach(GameObject teleporter in GameObject.FindGameObjectsWithTag("Teleporter"))
        {
            if (teleporter != gameObject)
                otherTeleporter = teleporter;
        }
	}
	
    /// <summary>
    /// Method that teleports the player to the other teleporter.
    /// </summary>
    /// <param name="player">The player that will be teleported.</param>
	public void teleport(GameObject player)
    {
        player.transform.position = otherTeleporter.transform.position + Vector3.up;
    }
}
