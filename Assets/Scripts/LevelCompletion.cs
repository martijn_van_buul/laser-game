﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using MovementEffects;
using System.Collections.Generic;

public class LevelCompletion : MonoBehaviour {

    public static LevelCompletion instance;
    private bool completed = false;

    void Awake()
    {
        instance = this;
    }

    /// <summary>
    /// Method when the level completes.
    /// </summary>
    public void LevelEnd()
    {
        if (!completed)
        {
            completed = true;
            InputManager.instance.SetMyPlayerMovement(null);

            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>().StopPlayer();

            Invoke("DisperseBlocks", 0.4f);
            Invoke("PlayExplosionSound", 0.32f);

            int levelNumber = SceneManager.GetActiveScene().buildIndex;

            if (!PlayerPrefs.HasKey("LevelCompleted" + (levelNumber - 1)))
                PlayerPrefs.SetInt("LevelCompleted" + (levelNumber - 1), 1);

            if (!PlayerPrefs.HasKey("Level" + (levelNumber - 1) + "Steps") || PlayerPrefs.GetInt("Level" + (levelNumber - 1) + "Steps") > GameManager.instance.amountSteps)
                PlayerPrefs.SetInt("Level" + (levelNumber - 1) + "Steps", GameManager.instance.amountSteps);
        }
    }

    void PlayExplosionSound()
    {
        GetComponent<AudioSource>().Play();
    }

    void DisperseBlocks()
    {
        Timing.RunCoroutine(CameraMovement.instance.Shake(0.6f, 0.6f));
        GameObject.FindGameObjectWithTag("Player").AddComponent<Rigidbody>();
        Timing.RunCoroutine(FadeManager.instance.FadeEndScene());
        Timing.RunCoroutine(AmbientSound.instance.FadeMusic(1.5f, false));
        UIBehaviour.instance.LevelDone();
        MoveBlocks.instance.Disperse();
    }
}
