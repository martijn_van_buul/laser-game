﻿using UnityEngine;
using System.Collections;

public class BreakableFloorBehaviour : MonoBehaviour {

    public int health;

    private Vector3 startPosition;
    private float explosionPower = 5.0f;

    private TextMesh textHealth;

    //Initialisation of the durabilty of the floor tile.
    void Start()
    {
        textHealth = GetComponentInChildren<TextMesh>();
        determineText();
    }

    //Checks to see if the floor is out of sight.
    void Update()
    {
        if (health == 0)
            if (transform.position.y < -25)
                Destroy(gameObject);
    }

    /// <summary>
    /// Method that decreases the durability of the object.
    /// Onec it hits zero the floor is not usable anymore and will drop.
    /// </summary>
    /// <param name="amount">The amount the health will be reduced with.</param>
    public void decreaseHealth(int amount)
    {
        if (health - amount <= 0)
        {
            health = 0;
            determineText();

            Rigidbody thisRigidbody = gameObject.AddComponent<Rigidbody>();
            thisRigidbody.AddForce(new Vector3(
                Random.Range(-explosionPower, explosionPower), 
                0, 
                Random.Range(-explosionPower, explosionPower)));
        }
        else
        {
            health -= amount;

            determineText();
        }
    }

    /// <summary>
    /// Method that sets the health to the durabilty of the floor.
    /// </summary>
    private void determineText()
    {
        textHealth.text = health.ToString();
    }
}
