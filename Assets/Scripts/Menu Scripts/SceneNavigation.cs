﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using MovementEffects;

public class SceneNavigation : MonoBehaviour {

    public static SceneNavigation instance;

    void Awake()
    {
        instance = this;
    }

    public void LoadScene(int levelId)
    {
        SceneManager.LoadScene(1);
    }
    /// <summary>
    /// Method for changing a level after clearing a level.
    /// </summary>
    /// <param name="levelId">The id of the current level.</param>
    public void ChangeLevel(int levelId)
    {
        Timing.KillCoroutines();

        levelId++;

        //Skipping boss level
        if(SceneManager.GetActiveScene().buildIndex != 0 && levelId % 5 == 0)
            levelId++;

        if (levelId < SceneManager.sceneCountInBuildSettings)
            SceneManager.LoadScene(levelId);
        else
            SceneManager.LoadScene(0);
    }
}
