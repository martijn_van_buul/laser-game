﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

public class SceneLoaderManager : MonoBehaviour {

    public static SceneLoaderManager instance;


    public GameObject[] Barrier1;
    public GameObject[] Barrier2;
    public GameObject[] Barrier3;
    public GameObject[] Barrier4;
    public GameObject[] Barrier5;
    public GameObject[] Barrier6;

    public Dictionary<string, int> levelSteps = new Dictionary<string, int>();


    void Awake()
    {
        instance = this;

        #region level Steps
        levelSteps.Add("level1", 4);
        levelSteps.Add("level2", 5);
        levelSteps.Add("level3", 7);
        levelSteps.Add("level4", 9);
        levelSteps.Add("level5", 7);
        levelSteps.Add("level6", 6);
        levelSteps.Add("level7", 10);
        levelSteps.Add("level8", 15);
        levelSteps.Add("level9", 15);
        levelSteps.Add("level10", 35);
        levelSteps.Add("level11", 16);
        levelSteps.Add("level12", 11);
        levelSteps.Add("level13", 11);
        levelSteps.Add("level14", 8);
        levelSteps.Add("level15", 30);
        levelSteps.Add("level16", 10);
        levelSteps.Add("level17", 18);
        levelSteps.Add("level18", 6);
        levelSteps.Add("level19", 9);
        levelSteps.Add("level20", 29);
        levelSteps.Add("level21", 2);
        levelSteps.Add("level22", 12);
        levelSteps.Add("level23", 7);
        levelSteps.Add("level24", 52);
        levelSteps.Add("level25", 74);
        levelSteps.Add("level26", 7);
        levelSteps.Add("level27", 11);
        levelSteps.Add("level28", 7);
        levelSteps.Add("level29", 12);
        levelSteps.Add("level30", 84);
        #endregion
    }

    void Start()
    {
        if (!PlayerPrefs.HasKey("FirstBarrier"))
        {
            int levelCompleteCount = 0;

            for (int i = 1; i <= 4; i++)
                if (PlayerPrefs.HasKey("LevelCompleted" + i))
                    levelCompleteCount++;

            if (levelCompleteCount >= 3)
            {
                PlayerPrefs.SetInt("FirstBarrier", 1);
                foreach (GameObject barrier in Barrier1)
                    Timing.RunCoroutine(RiseBarrier(barrier.transform));
            }
        }
        else
            foreach (GameObject barrier in Barrier1)
                barrier.transform.position += Vector3.up * 100;

        if (!PlayerPrefs.HasKey("SecondBarrier"))
        {
            int levelCompleteCount = 0;

            for (int i = 6; i <= 9; i++)
                if (PlayerPrefs.HasKey("LevelCompleted" + i))
                    levelCompleteCount++;

            if (levelCompleteCount >= 3)
            {
                PlayerPrefs.SetInt("SecondBarrier", 1);
                foreach (GameObject barrier in Barrier2)
                    Timing.RunCoroutine(RiseBarrier(barrier.transform));
            }
        }
        else
            foreach (GameObject barrier in Barrier2)
                barrier.transform.position += Vector3.up * 100;

        if (!PlayerPrefs.HasKey("ThirdBarrier"))
        {
            int levelCompleteCount = 0;

            for (int i = 11; i <= 14; i++)
                if (PlayerPrefs.HasKey("LevelCompleted" + i))
                    levelCompleteCount++;

            if (levelCompleteCount >= 3)
            {
                PlayerPrefs.SetInt("ThirdBarrier", 1);
                foreach (GameObject barrier in Barrier3)
                    Timing.RunCoroutine(RiseBarrier(barrier.transform));
            }
        }
        else
            foreach (GameObject barrier in Barrier3)
                barrier.transform.position += Vector3.up * 100;

        if (!PlayerPrefs.HasKey("FourthBarrier"))
        {
            int levelCompleteCount = 0;

            for (int i = 16; i <= 19; i++)
                if (PlayerPrefs.HasKey("LevelCompleted" + i))
                    levelCompleteCount++;

            if (levelCompleteCount >= 3)
            {
                PlayerPrefs.SetInt("FourthBarrier", 1);
                foreach (GameObject barrier in Barrier4)
                    Timing.RunCoroutine(RiseBarrier(barrier.transform));
            }
        }
        else
            foreach (GameObject barrier in Barrier4)
                barrier.transform.position += Vector3.up * 100;

        if (!PlayerPrefs.HasKey("FifthBarrier"))
        {
            int levelCompleteCount = 0;

            for (int i = 21; i <= 24; i++)
                if (PlayerPrefs.HasKey("LevelCompleted" + i))
                    levelCompleteCount++;

            if (levelCompleteCount >= 3)
            {
                PlayerPrefs.SetInt("FifthBarrier", 1);
                foreach (GameObject barrier in Barrier5)
                    Timing.RunCoroutine(RiseBarrier(barrier.transform));
            }
        }
        else
            foreach (GameObject barrier in Barrier5)
                barrier.transform.position += Vector3.up * 100;

        if (!PlayerPrefs.HasKey("SixthBarrier"))
        {
            int levelCompleteCount = 0;

            for (int i = 26; i <= 29; i++)
                if (PlayerPrefs.HasKey("LevelCompleted" + i))
                    levelCompleteCount++;

            if (levelCompleteCount >= 3)
            {
                PlayerPrefs.SetInt("SixthBarrier", 1);
                foreach (GameObject barrier in Barrier6)
                    Timing.RunCoroutine(RiseBarrier(barrier.transform));
            }
        }
        else
            foreach (GameObject barrier in Barrier6)
                barrier.transform.position += Vector3.up * 100;
    }

    IEnumerator<float> RiseBarrier(Transform block)
    {
        Vector3 initialPosition = block.position + 100 * Vector3.up;

        float speed = Random.Range(3.5f, 5.5f);

        yield return Timing.WaitForOneFrame;

        while ((block.position.y < (initialPosition.y - 0.02f)))
        {
            block.transform.position += Vector3.up * (initialPosition.y - block.transform.position.y + 0.3f) * speed * Time.deltaTime;
            yield return Timing.WaitForOneFrame;
        }

        block.position = initialPosition;
    }
}
