﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour {

    public int LevelNumber;
    public Material LevelCompletedMaterial;

    void Start()
    {
        if (LevelNumber != 0)
        {
            GetComponentInChildren<TextMesh>().text = LevelNumber.ToString();
            if(PlayerPrefs.HasKey("LevelCompleted" + (LevelNumber - 1))) {
                GetComponent<Renderer>().material = LevelCompletedMaterial;
            }

            if(PlayerPrefs.HasKey("Level" + (LevelNumber - 1) + "Steps"))
            {
                transform.GetChild(0).GetChild(0).gameObject.SetActive(true);

                int amountSteps = PlayerPrefs.GetInt("Level" + (LevelNumber - 1) + "Steps");
                int firstGoalStep = 0;
                SceneLoaderManager.instance.levelSteps.TryGetValue("level" + LevelNumber, out firstGoalStep);

                if (amountSteps <= firstGoalStep * 2)
                    transform.GetChild(1).GetChild(0).gameObject.SetActive(true);
                if(amountSteps <= firstGoalStep)
                    transform.GetChild(2).GetChild(0).gameObject.SetActive(true);
            }
        }


    }

	public void SelectLevel()
    {
        SceneNavigation.instance.ChangeLevel(LevelNumber);
    }
}
