﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System.Linq;
using UnityEngine.SceneManagement;

public class LevelSelection : MonoBehaviour {

    public GameObject levelButton;

	void Start () {
        //levels = Directory.GetFiles("Assets/Scenes/Levels").Where(info => !info.Contains(".meta")).ToArray();

        for(int count = 1; count < 6; count++)
        {
            GameObject goButton = (GameObject)Instantiate(levelButton);
            goButton.transform.SetParent(transform, false);
            goButton.transform.localScale = new Vector3(1, 1, 1);
            goButton.transform.position = new Vector3(0, 30 -count * 10, 90);

            Button tempButton = goButton.GetComponent<Button>();

            string level = "Scenes/Levels/SceneLevel" + count;
            tempButton.onClick.AddListener(() => changeLevel(level));

            Text tempText = goButton.GetComponentInChildren<Text>();
            tempText.text = count.ToString();
        }
    }

    /// <summary>
    /// Preparing the scene for loading by editing it's name.
    /// </summary>
    /// <param name="sceneName">The name of the scene.</param>
    private void changeLevel(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
