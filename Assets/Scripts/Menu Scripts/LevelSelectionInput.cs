﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectionInput : MonoBehaviour {

    RaycastHit hit;

    private void Start()
    {
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
    }

    void Update () {
        foreach (Touch touch in Input.touches)
            if (touch.phase == TouchPhase.Ended)
                CheckLevelSelected(touch.position);

        if(Input.GetMouseButtonUp(0))
            CheckLevelSelected(Input.mousePosition);

    }

    private void CheckLevelSelected(Vector2 position)
    {
        if (Physics.Raycast(Camera.main.ScreenPointToRay(position), out hit, 20))
            if (hit.collider.GetComponent<LevelLoader>() != null)
                hit.collider.GetComponent<LevelLoader>().SelectLevel();
    }
}
