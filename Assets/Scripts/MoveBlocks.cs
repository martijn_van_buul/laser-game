﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

public class MoveBlocks : MonoBehaviour {

    public static MoveBlocks instance;
    private GameObject startBlock;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        startBlock = (GameObject)GameObject.FindGameObjectWithTag("Respawn");
    }
	
	public void Disperse()
    {
        foreach(Transform block in transform)
        {
            if (!block.GetComponent<Rigidbody>())
                block.gameObject.AddComponent<Rigidbody>();
            else
            {
                block.GetComponent<Rigidbody>().isKinematic = false;
                block.GetComponent<Rigidbody>().useGravity = true;
            }
            block.GetComponent<Rigidbody>().AddExplosionForce(200, GameObject.FindGameObjectWithTag("Player").transform.position, 10);
        }
    }

    public IEnumerator<float> DisperseAtStart(Transform player)
    {
        List<Transform> transforms = new List<Transform>();
        List<Transform> toBeRemoved = new List<Transform>();

        foreach (Transform block in transform)
            if(block != player && block != startBlock.transform)
                transforms.Add(block);

        float distance = 1;
        while (transforms.Count > 0 && distance < 3f)
        {
            distance += Time.deltaTime * 14;

            foreach (Transform block in transforms)
                if (Vector3.Distance(block.position, startBlock.transform.position) < distance)
                {
                    toBeRemoved.Add(block);
                    Timing.RunCoroutine(RaiseBlock(block, distance));
                }

            foreach (Transform block in toBeRemoved)
                transforms.Remove(block);

            yield return Timing.WaitForOneFrame;
        }
    }

    private IEnumerator<float> RaiseBlock(Transform block, float distance)
    {
        Vector3 initialPosition = block.position;

        float offset = 0.5f / (distance * 1.2f);
        
        while (block.position.y < initialPosition.y + offset)
        {
            block.position += (initialPosition.y + (offset + 0.1f) - block.position.y) * Vector3.up * Time.deltaTime * 12;
            yield return Timing.WaitForOneFrame;
        }

        while (block.position.y > initialPosition.y)
        {
            block.position += (initialPosition.y - 0.1f - block.position.y) * Vector3.up * Time.deltaTime * 12;
            yield return Timing.WaitForOneFrame;
        }

        block.position = initialPosition;
    }
}
