﻿using MovementEffects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FadeImage : MonoBehaviour {

    [SerializeField]
    float delay;


	// Use this for initialization
	IEnumerator Start () {
        yield return new WaitForSeconds(delay);

        Image fadeImage = GetComponent<Image>();

        fadeImage.canvasRenderer.SetAlpha(0);
        fadeImage.color = new Color(0, 0, 0, 1);
        fadeImage.CrossFadeAlpha(1, 0.3f, false);

        yield return new WaitForSeconds(0.3f);

        SceneManager.LoadScene(1);
    }

}
