﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MovementEffects;
using System.Linq;
using System;

public class ScreenTransition : MonoBehaviour {

    private const string AndroidRatingURI = "http://play.google.com/store/apps/details?id={0}";
    private const string iOSRatingURI = "itms://itunes.apple.com/us/app/apple-store/{0}?mt=8";

    [Tooltip("iOS App ID (number), example: 1122334455")]
    public string iOSAppID = "";

    private string url;

    public RectTransform playImageTransform;
    public Text testText;
    public Image soundImage;

    bool isMuted;
    float targetFill = 0.95f;

    bool isMovingTop = true;
    float rotationToGo;

    Vector3 topPosition = new Vector3(3.5f, 12, 70);
    Vector3 downPosition = new Vector3(3.5f, 12, -5);
    Vector3 targetPosition = new Vector3(3.5f, 12, 70);

    private List<TouchStruct> activeTouches;




    private void Start()
    {
        activeTouches = new List<TouchStruct>();


        if (PlayerPrefs.GetInt("Muted") == 1)
        {
            targetFill = 0.56f;
            soundImage.fillAmount = 0.56f;
            isMuted = true;
        }

        AudioListener.volume = (soundImage.fillAmount - 0.55f) * (1 / 0.4f);


#if UNITY_IOS
        if (!string.IsNullOrEmpty (iOSAppID)) {
            url = iOSRatingURI.Replace("{0}",iOSAppID);
        }
        else {
            Debug.LogWarning ("Please set iOSAppID variable");
        }
 
#elif UNITY_ANDROID
        url = AndroidRatingURI.Replace("{0}", Application.identifier);
#endif
    }

    // Update is called once per frame
    void Update () {
        #region moving screen up and down
        if (isMovingTop && Input.GetAxisRaw("Vertical") < 0)
        {
            isMovingTop = false;
            targetPosition = downPosition;
            rotationToGo += 90;
        }
        else if(!isMovingTop && Input.GetAxisRaw("Vertical") > 0)
        {
            isMovingTop = true;
            targetPosition = topPosition;
            rotationToGo -= 90;
        }

        foreach (Touch touch in Input.touches)
        {
            switch (touch.phase)
            {

                case TouchPhase.Began:
                    activeTouches.Add(new TouchStruct(touch.fingerId, touch.position, Time.time));
                    break;
                case TouchPhase.Ended:
                    TouchStruct touchStart = activeTouches.Single(t => t.TouchIndex == touch.fingerId);
                    if ((touch.position - touchStart.TouchPosition).y > 25  && isMovingTop)
                    {
                        isMovingTop = false;
                        targetPosition = downPosition;
                        rotationToGo += 90;
                    }
                    else if ((touch.position - touchStart.TouchPosition).y < -25 && !isMovingTop)
                    {
                        isMovingTop = true;
                        targetPosition = topPosition;
                        rotationToGo -= 90;
                    }

                    activeTouches.Remove(touchStart);
                    break;
            }
        }

        transform.position += Mathf.Sign(targetPosition.z - transform.position.z) * Mathf.Min(Mathf.Abs(targetPosition.z - transform.position.z) , Mathf.Max(0.05f, ((Mathf.Min(Vector3.Distance(topPosition, transform.position), Vector3.Distance(transform.position, downPosition))))) * 1.1f) * Vector3.forward * 10 * Time.deltaTime;

        playImageTransform.eulerAngles -= rotationToGo * 10 * Vector3.forward * Time.deltaTime;
        rotationToGo -= rotationToGo * 10 * Time.deltaTime;
        #endregion

        soundImage.fillAmount += (targetFill - soundImage.fillAmount) * 8 * Time.deltaTime;
        AudioListener.volume = (soundImage.fillAmount - 0.55f) * (1/ 0.4f);

    }

    public void ToggleSound()
    {
        isMuted = !isMuted;
        if (isMuted)
            targetFill = 0.55f;
        else
            targetFill = 0.95f;

        PlayerPrefs.SetInt("Muted", Convert.ToInt32(isMuted));
    }

    public void PlayGame()
    {
        if (isMovingTop)
        {
            isMovingTop = false;
            targetPosition = downPosition;
            rotationToGo += 90;
        }
    }

    public void RateGame()
    {
        if (!string.IsNullOrEmpty(url))
        {
            Application.OpenURL(url);
        }
        else
        {
            Debug.LogWarning("Unable to open URL, invalid OS");
        }
    }
}
