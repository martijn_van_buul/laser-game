﻿using UnityEngine;
using System.Collections;

public class ReceptorManager : MonoBehaviour {

    public float chargedValue;
    private Material material;
    private bool isCharging = false;
    private AudioSource myAudioSource;
    private Light myLight;

    private float currentVolume;
    private float targetVolume;
    private float maxVolume = 0.36f;
    private float volumeAcceleration = 12;

    void Start()
    {
        myAudioSource = GetComponent<AudioSource>();
        myLight = GetComponent<Light>();

        material = GetComponent<Renderer>().material;
    }

    void Update()
    {
        //Keeps the charge in check.
        if (isCharging)
        {
            targetVolume = maxVolume;
            chargedValue++;
        }
        else
        {
            targetVolume = 0;
            chargedValue--;
        }

        chargedValue = Mathf.Clamp(chargedValue, 0, 100);

        myAudioSource.volume = currentVolume;
        myAudioSource.pitch = chargedValue / 100f * 1.2f;

        currentVolume = currentVolume + (targetVolume - currentVolume) * volumeAcceleration * Time.deltaTime;

        myLight.intensity = chargedValue / 100f;
        material.color = new Color(1, 
            Mathf.Min((float)225 / 255, 1 - (2.25f * chargedValue / 255)), 
            Mathf.Min((float)225 / 255, 1 - (2.25f * chargedValue / 255)));

        isCharging = false;

        if (chargedValue == 100)
            transform.GetChild(0).gameObject.SetActive(true);
        else
            transform.GetChild(0).gameObject.SetActive(false);

    }

    /// <summary>
    /// Method for increasing the charde of the receptor.
    /// </summary>
    /// <param name="value">The charge value will be increased by this value.</param>
	public void increaseCharge(Vector3 direction)
    {
        isCharging = true;
    }
}
