﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    [Header("Input Keycodes")]
    public KeyCode respawnKey = KeyCode.Space;
    public KeyCode resetKey = KeyCode.R;

    [Header("Visual assets")]
    public GameObject InputCanvas;
    public Text StepCounter;

    public delegate void PlayerRespawn();
    public PlayerRespawn playerRespawned;

    public static GameManager instance;

    public int amountSteps;
    public int amountLives = 2;

    private Vector3 startPosition;

    private GameObject player;
    private GameObject myPlayer;

    public Dictionary<string, int> levelSteps = new Dictionary<string, int>();

    public static bool LevelFinished;

    void Awake()
    {
        LevelFinished = false;
        instance = this;

        player = (GameObject)Resources.Load("Prefabs/Player");
        InputCanvas = GameObject.FindGameObjectWithTag("Canvas");
        startPosition = GameObject.FindGameObjectWithTag("Respawn").transform.position + new Vector3(0, 1, 0);

        myPlayer = (GameObject)Instantiate(player, startPosition, Quaternion.identity);

    }

    void Start()
    {
        #region level Steps
        levelSteps.Add("level1", 4);
        levelSteps.Add("level2", 5);
        levelSteps.Add("level3", 7);
        levelSteps.Add("level4", 9);
        levelSteps.Add("level5", 7);
        levelSteps.Add("level6", 6);
        levelSteps.Add("level7", 10);
        levelSteps.Add("level8", 15);
        levelSteps.Add("level9", 15);
        levelSteps.Add("level10", 35);
        levelSteps.Add("level11", 16);
        levelSteps.Add("level12", 11);
        levelSteps.Add("level13", 11);
        levelSteps.Add("level14", 8);
        levelSteps.Add("level15", 30);
        levelSteps.Add("level16", 10);
        levelSteps.Add("level17", 18);
        levelSteps.Add("level18", 6);
        levelSteps.Add("level19", 9);
        levelSteps.Add("level20", 29);
        levelSteps.Add("level21", 2);
        levelSteps.Add("level22", 12);
        levelSteps.Add("level23", 7);
        levelSteps.Add("level24", 52);
        levelSteps.Add("level25", 74);
        levelSteps.Add("level26", 7);
        levelSteps.Add("level27", 11);
        levelSteps.Add("level28", 7);
        levelSteps.Add("level29", 12);
        levelSteps.Add("level30", 84);
        #endregion

        if (MoveBlocks.instance != null)
            myPlayer.transform.parent = MoveBlocks.instance.transform;

        myPlayer.GetComponentInChildren<Animator>().SetTrigger("Spawn");
    }

    void Update()
    {
        if (Input.GetKeyDown(respawnKey))
            RespawnPlayer();
        else if (Input.GetKeyDown(resetKey))
            RestartLevel();
    }

    /// <summary>
    /// Method for restarting the current level.
    /// </summary>
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Method for exiting to the level selection menu.
    /// </summary>
    public void ExitLevel()
    {
        SceneManager.LoadScene(0);
    }

    /// <summary>
    /// Method for updating the elements outside the player.
    /// </summary>
    void updatePlayer()
    {
        myPlayer.GetComponentInChildren<Animator>().SetTrigger("Spawn");
        playerRespawned();
    }

    /// <summary>
    /// Method that checks if the player has left his spawn place and if he did a new player is instantiated.
    /// </summary>
    /// <returns>Returns true if the player was successfully instantiated</returns>
    public void RespawnPlayer()
    {
        if (myPlayer.GetComponent<PlayerMovement>().doneMoving)
        {
            if (myPlayer.transform.position != startPosition)
            {
                if (UseLife())
                {
                    myPlayer.tag = "Deflector";
                    myPlayer.name = "Deflector";

                    myPlayer.transform.GetChild(0).tag = "Deflector";
                    myPlayer.transform.GetChild(0).name = "Deflector";

                    Destroy(myPlayer.GetComponent<PlayerMovement>());

                    myPlayer = (GameObject)Instantiate(player, startPosition, Quaternion.identity);
                    if(MoveBlocks.instance != null)
                        myPlayer.transform.parent = MoveBlocks.instance.transform;

                    updatePlayer();
                }
            }
        }
    }

    /// <summary>
    /// Method for adding and counting steps the player takes.
    /// </summary>
    public void AddStep()
    {
        amountSteps++;
        UIBehaviour.instance.StepAdded(amountSteps);
    }

    /// <summary>
    /// Method for checking if the player has any lives left and if that is the case it will take one.
    /// </summary>
    /// <returns>true if the player does have lives remaining and false if he does not.</returns>
    public bool UseLife()
    {
        if (amountLives-- > 0)
            return true;
        else
            return false;
    }
}
