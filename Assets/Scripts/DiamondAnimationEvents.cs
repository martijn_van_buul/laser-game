﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MovementEffects;

public class DiamondAnimationEvents : MonoBehaviour {

    public static DiamondAnimationEvents instance;

    public Image FirstDiamond;
    public Image SecondDiamond;
    public Image ThirdDiamond;

    public Image FirstInnerDiamond;
    public Image SecondInnerDiamond;
    public Image ThirdInnerDiamond;

    void Awake()
    {
        instance = this;
    }

    public IEnumerator<float> LevelDone(int amountDiamonds)
    {
        yield return Timing.WaitForOneFrame;

        FirstDiamond.gameObject.SetActive(true);
        FirstDiamond.canvasRenderer.SetAlpha(0);
        FirstDiamond.CrossFadeAlpha(1, 0.5f, false);

        SecondDiamond.gameObject.SetActive(true);
        SecondDiamond.canvasRenderer.SetAlpha(0);
        SecondDiamond.CrossFadeAlpha(1, 0.5f, false);

        ThirdDiamond.gameObject.SetActive(true);
        ThirdDiamond.canvasRenderer.SetAlpha(0);
        ThirdDiamond.CrossFadeAlpha(1, 0.5f, false);

        yield return Timing.WaitForSeconds(0.5f);

        Timing.RunCoroutine(fillDiamond(FirstInnerDiamond, FirstDiamond, -1));
        yield return Timing.WaitForSeconds(0.25f);

        if (amountDiamonds >= 2)
        {
            Timing.RunCoroutine(fillDiamond(SecondInnerDiamond, SecondDiamond, 0));
            yield return Timing.WaitForSeconds(0.25f);
        }
        if(amountDiamonds == 3)
        {
            Timing.RunCoroutine(fillDiamond(ThirdInnerDiamond, ThirdDiamond, 1));
            yield return Timing.WaitForSeconds(0.25f);
        }
        yield return Timing.WaitForSeconds(0.25f);

        FirstDiamond.CrossFadeAlpha(0, 0.5f, false);
        SecondDiamond.CrossFadeAlpha(0, 0.5f, false);
        ThirdDiamond.CrossFadeAlpha(0, 0.5f, false);

        FirstInnerDiamond.CrossFadeAlpha(0, 0.5f, false);
        SecondInnerDiamond.CrossFadeAlpha(0, 0.5f, false);
        ThirdInnerDiamond.CrossFadeAlpha(0, 0.5f, false);

        yield return Timing.WaitForSeconds(0.5f);
    }

    IEnumerator<float> fillDiamond(Image diamond, Image outerDiamond, int pitchChange)
    {
        while(diamond.fillAmount < 1)
        {
            diamond.fillAmount += (0.25f + (diamond.fillAmount * 2)) * 5 * Time.deltaTime;
            yield return Timing.WaitForOneFrame;
        }

        outerDiamond.GetComponent<AudioSource>().pitch = 1 + pitchChange * 0.15f;
        outerDiamond.GetComponent<AudioSource>().Play();
        float shake = 0.5f;
        Vector3 initialPosition = outerDiamond.GetComponent<RectTransform>().anchoredPosition;


        while (shake > 0)
        {
            outerDiamond.GetComponent<RectTransform>().anchoredPosition = initialPosition + (Random.onUnitSphere + new Vector3(0.2f, 0.2f, 0)) * shake * 30f;
            shake -= Time.deltaTime;
            yield return Timing.WaitForOneFrame;
        }

        outerDiamond.GetComponent<RectTransform>().anchoredPosition = initialPosition;

    }

}
