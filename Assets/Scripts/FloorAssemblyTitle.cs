﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class FloorAssemblyTitle : MonoBehaviour {

	// Use this for initialization
	void Start () {
        foreach (Transform block in transform)
        {
            if (!block.CompareTag("Player"))
                Timing.RunCoroutine(MoveBlock(block, true));
        }
    }

    /// <summary>
    /// Method for moving level blocks up and down.
    /// Specifically moving everything but buttons up and buttons down.
    /// </summary>
    /// <param name="block">The transform theat will be moved.</param>
    /// <param name="isGoingUp">Determines if it goes up or down.</param>
    /// <returns></returns>
    IEnumerator<float> MoveBlock(Transform block, bool isGoingUp)
    {
        Vector3 initialPosition = block.position;

        Vector3 displacement;


        if (Physics.Raycast(new Ray(block.position, -Vector3.up), 1))
            displacement = Vector3.up * Random.Range(130, 170);
        else if (Physics.Raycast(new Ray(block.position, Vector3.up), 1))
            displacement = Vector3.up * Random.Range(330, 370);
        else
            displacement = Vector3.up * Random.Range(170, 330);

        yield return Timing.WaitForOneFrame;

        if (isGoingUp)
            block.position -= displacement;
        else
            block.position += displacement;

        while ((isGoingUp && block.position.y < (initialPosition.y - 0.02f)) || (!isGoingUp && block.position.y > (initialPosition.y + 0.02f)))
        {
            block.transform.position += Vector3.up * (initialPosition.y - block.transform.position.y + 0.3f) * 5.5f * Time.deltaTime;
            yield return Timing.WaitForOneFrame;
        }

        block.position = initialPosition;
    }
}
