﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MovementEffects;
using System.Collections.Generic;

public class PlayerMovement : MonoBehaviour {

    private float movementTime = 0.2f;
    private int nextMovement = -1;
    [HideInInspector]
    public bool doneMoving = true;

    private float width = 1;
    private float height = 1;

    private GameObject currentFloor;
    private Ray floorRay;
    private RaycastHit hit;

    private Animator myAnimator;

    private AudioSource myAudioSource;
    private AudioClip[] walkingSounds;

    private GameObject PlayerParticles;
    private ParticleSystem playerParticlesSystem;
    private int particleAmount = 20;

	void Start () {
        Application.targetFrameRate = 1000;
        myAnimator = GetComponentInChildren<Animator>();
        myAudioSource = GetComponent<AudioSource>();

        PlayerParticles = GameObject.FindGameObjectWithTag("PlayerParticles");
        playerParticlesSystem = PlayerParticles.GetComponent<ParticleSystem>();

        walkingSounds = new AudioClip[5];
        walkingSounds[0] = (AudioClip)Resources.Load("Sounds/WalkSoundStone-01");
        walkingSounds[1] = (AudioClip)Resources.Load("Sounds/WalkSoundStone-02");
        walkingSounds[2] = (AudioClip)Resources.Load("Sounds/WalkSoundStone-03");
        walkingSounds[3] = (AudioClip)Resources.Load("Sounds/WalkSoundStone-04");
        walkingSounds[4] = (AudioClip)Resources.Load("Sounds/WalkSoundStone-05");

        floorRay = new Ray(transform.position, new Vector3(0, -1, 0));

        if (Physics.Raycast(floorRay, out hit, 1))
                currentFloor = hit.collider.gameObject;

        doneMoving = true;
    }


    public void Movement(int direction)
    {
        //Checking if the player wants to move the player-object.
        if (myAnimator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            if (doneMoving)
            {
                if (direction == 2 && DetectCollision(new Vector3(0, 0, 1)))
                    Timing.RunCoroutine(movePlayer(transform.position + new Vector3(0, -0.5f * height, 0.5f * height), new Vector3(1, 0, 0)));
                else if (direction == 1 && DetectCollision(new Vector3(0, 0, -1)))
                    Timing.RunCoroutine(movePlayer(transform.position + new Vector3(0, -0.5f * height, -0.5f * height), new Vector3(-1, 0, 0)));
                else if (direction == 3 && DetectCollision(new Vector3(1, 0, 0)))
                    Timing.RunCoroutine(movePlayer(transform.position + new Vector3(0.5f * width, -0.5f * height, 0), new Vector3(0, 0, -1)));
                else if (direction == 0 && DetectCollision(new Vector3(-1, 0, 0)))
                    Timing.RunCoroutine(movePlayer(transform.position + new Vector3(-0.5f * width, -0.5f * height, 0), new Vector3(0, 0, 1)));
            }
            else
                nextMovement = direction;
        }
    }

    /// <summary>
    /// Moves the player around an axis around a point.
    /// </summary>
    /// <param name="point">The point it rotates arund</param>
    /// <param name="axis">The axis it rotates around.</param>
    /// <returns>Returns IEnumerator</returns>
    private IEnumerator<float> movePlayer(Vector3 point, Vector3 axis)
    {
        doneMoving = false;

        //Adding a step to the counter of this level
        GameManager.instance.AddStep();

        //Playing the audio that a step makes
        //TODO: make multiple step sounds and play a random one.
        myAudioSource.PlayOneShot(walkingSounds[Random.Range(0, walkingSounds.Length)]);

        //myAudioSource.PlayOneShot(walkingSound);
        //myAudioSource.pitch = Random.Range(0.8f, 1.2f);

        float counter = 0;
        bool smokeEmitted = false;

        while (counter < 90)
        {
            if(!((counter + 90 / (movementTime / Time.deltaTime)) > 90))
                transform.RotateAround(point, axis, (90 / (movementTime / Time.deltaTime)));

            counter += (90 / (movementTime / Time.deltaTime));

            //Making smoke particles appear
            if (!smokeEmitted && counter > 65)
            {
                EmitParticles();
                smokeEmitted = true;
            }

            yield return Timing.WaitForOneFrame;
        }

        //Rounding the rotation to the nearest 90
        transform.eulerAngles = roundRotations(transform.eulerAngles);
        transform.position = new Vector3(Mathf.Round(transform.position.x), Mathf.Round(transform.position.y), Mathf.Round(transform.position.z));

        doneMoving = true;

        if (nextMovement != -1)
        {
            Movement(nextMovement);
            nextMovement = -1;
        }

        if (currentFloor.tag == "Teleporter")
            currentFloor.GetComponent<TeleporterManager>().teleport(gameObject);
    }

    /// <summary>
    /// Rounds down the angles towards 90 degrees.
    /// </summary>
    /// <param name="eulerAngler"> The vector that is to be rounded</param>
    /// <returns>returns rounded vector</returns>
    private Vector3 roundRotations(Vector3 eulerAngler)
    {
        Vector3 roundedVector = eulerAngler;
        roundedVector.x = Mathf.Round(roundedVector.x / 90) * 90;
        roundedVector.y = Mathf.Round(roundedVector.y / 90) * 90;
        roundedVector.z = Mathf.Round(roundedVector.z / 90) * 90;
        return roundedVector;
    }

    /// <summary>
    /// Checks if the player will still be on the grid after moving.
    /// If this is true the position in the grid is also altered.
    /// </summary>
    /// <param name="direction">The direction the player is moving</param>
    /// <returns></returns>
    private bool DetectCollision(Vector3 direction)
    {
        floorRay = new Ray(transform.position, direction);

        if (!Physics.Raycast(floorRay, out hit, 1, ~(1 << LayerMask.NameToLayer("PlayerOnly"))))
        {
            floorRay = new Ray(transform.position + direction, new Vector3(0, -1, 0));

            if (Physics.Raycast(floorRay, out hit, 0.5f))
            {
                if (currentFloor.tag == "Breakable Floor")
                    currentFloor.GetComponent<BreakableFloorBehaviour>().decreaseHealth(1);
                else
                    if(hit.collider.GetComponent<FloorBehaviour>() != null)
                        hit.collider.GetComponent<FloorBehaviour>().StepOn();

                currentFloor = hit.collider.gameObject;
                return true;
            }
         }
        
        return false;
    }

    /// <summary>
    /// Method for emitting particles below the player.
    /// </summary>
    public void EmitParticles()
    {
        PlayerParticles.transform.position = transform.position - new Vector3(0, 0.5f, 0);
        playerParticlesSystem.Emit(particleAmount);
    }

    /// <summary>
    /// Method for stopping the player's movement.
    /// </summary>
    public void StopPlayer()
    {
        nextMovement = -1;
    }
}
