﻿using UnityEngine;
using System.Collections;

public class DeflectorManager : MonoBehaviour {


    /// <summary>
    /// If the deflector is not hitting anything, it will shoot into the depths.
    /// </summary>
    /// <param name="origin">The place the deflector is hit from</param>
    public Vector3 NoHit(Vector3 origin)
    {
        Vector3 direction = Vector3.Normalize(transform.position - origin);

        if (Vector3.SqrMagnitude(direction + transform.forward) < 0.001f)
        {
            return 35 * transform.up + transform.position;
        }
        else if (Vector3.SqrMagnitude(direction + transform.up) < 0.001f)
        {
            return 35 * transform.forward + transform.position;
        }

        return Vector3.zero;
    }

    /// <summary>
    /// Function that determines if the laser is hitting it on it's reflection side.
    /// If It is hitting a reflection side the direction of the bounce is returned.
    /// </summary>
    /// <param name="origin">The place the deflector is hit from</param>
    /// <returns>Returns the direction of the reflection.</returns>
    public Vector3 ReflectVector(Vector3 origin)
    {
        Vector3 direction = Vector3.Normalize(transform.position - origin);

        if (Vector3.SqrMagnitude(direction + transform.forward) < 0.001f)
        {
            return transform.up;
        }
        else if (Vector3.SqrMagnitude(direction + transform.up) < 0.001f)
        {
            return transform.forward;
        }
        else
        {
            return Vector3.zero;
        }
            
    }
}
