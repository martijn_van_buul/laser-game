﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MovementEffects;

public class UIBehaviour : MonoBehaviour {

    public static UIBehaviour instance;

    public Image FirstDiamondBorder;
    public Image SecondDiamondBorder;
    public Image ThirdDiamondBorder;

    public Image FirstDiamond;
    public Image SecondDiamond;
    public Image ThirdDiamond;

    public Image FillSecondDiamond;
    public Image FillThirdDiamond;

    private float targetFillThird;
    private float targetFillSecond;

    private int firstGoalStep;

    private bool isThirdRemoved;
    private bool isSecondRemoved;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
    }

	public void LevelDone()
    {
        if (!isThirdRemoved)
        {
            Timing.RunCoroutine(removeFill(FillThirdDiamond));
            Timing.RunCoroutine(DiamondAnimationEvents.instance.LevelDone(3));
        }
        else if (!isSecondRemoved)
        {
            Timing.RunCoroutine(removeFill(FillSecondDiamond));
            Timing.RunCoroutine(DiamondAnimationEvents.instance.LevelDone(2));
        }
        else
            Timing.RunCoroutine(DiamondAnimationEvents.instance.LevelDone(1));



        isThirdRemoved = true;
        isSecondRemoved = true;


        foreach (Animator animator in GetComponentsInChildren<Animator>())
            if(animator.transform.parent == transform)
                animator.SetTrigger("LevelDone");

        Timing.RunCoroutine(FadeOut());
    }

    IEnumerator<float> FadeOut()
    {
        yield return Timing.WaitForSeconds(2.5f);

        FirstDiamond.CrossFadeAlpha(0, 0.5f, false);
        SecondDiamond.CrossFadeAlpha(0, 0.5f, false);
        ThirdDiamond.CrossFadeAlpha(0, 0.5f, false);

        FirstDiamondBorder.CrossFadeAlpha(0, 0.5f, false);
        SecondDiamondBorder.CrossFadeAlpha(0, 0.5f, false);
        ThirdDiamondBorder.CrossFadeAlpha(0, 0.5f, false);

        yield return Timing.WaitForSeconds(0.5f);
    }

    void Update()
    {
        if (!isThirdRemoved)
        {
            if (FillThirdDiamond.fillAmount < targetFillThird)
                FillThirdDiamond.fillAmount += (targetFillThird - FillThirdDiamond.fillAmount + 0.05f) * Time.deltaTime * 5;
            else
                FillThirdDiamond.fillAmount = targetFillThird;
        }
        else if(!isSecondRemoved)
        {
            if (FillSecondDiamond.fillAmount < targetFillSecond)
                FillSecondDiamond.fillAmount += (targetFillSecond - FillSecondDiamond.fillAmount + 0.05f) * Time.deltaTime * 5;
            else
                FillSecondDiamond.fillAmount = targetFillSecond;
        }
    }

    public void StepAdded(int stepAmount)
    {
        if(firstGoalStep == 0)
            GameManager.instance.levelSteps.TryGetValue("level" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex, out firstGoalStep);

        if (stepAmount <= firstGoalStep)
            targetFillThird = (float)stepAmount / (firstGoalStep);
        else if(stepAmount == firstGoalStep + 1)
        {
            Timing.RunCoroutine(RemoveDiamond(ThirdDiamond, FillThirdDiamond));
            isThirdRemoved = true;
        }

        if (stepAmount <= firstGoalStep * 2)
            targetFillSecond = (float)(stepAmount - firstGoalStep) / (firstGoalStep);
        else if (stepAmount == firstGoalStep * 2 + 1)
        {
            Timing.RunCoroutine(RemoveDiamond(SecondDiamond, FillSecondDiamond));
            isSecondRemoved = true;
        }
    }

    private IEnumerator<float> RemoveDiamond(Image diamond, Image diamondFill)
    {
        diamondFill.fillMethod = Image.FillMethod.Vertical;
        diamondFill.fillOrigin = 1;

        while (diamond.fillAmount > 0)
        {
            diamond.fillAmount -= (diamond.fillAmount + 0.1f) * Time.deltaTime * 8;
            yield return Timing.WaitForOneFrame;
        }

        while (diamondFill.fillAmount > 0)
        {
            diamondFill.fillAmount -= (diamondFill.fillAmount + 0.1f) * Time.deltaTime * 8;
            yield return Timing.WaitForOneFrame;
        }
    }

    private IEnumerator<float> removeFill(Image fillDiamond)
    {
        while (fillDiamond.fillAmount > 0)
        {
            fillDiamond.fillAmount -= (fillDiamond.fillAmount + 0.1f) * Time.deltaTime * 8;
            yield return Timing.WaitForOneFrame;
        }
    }
}
