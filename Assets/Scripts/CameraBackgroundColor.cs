﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBackgroundColor : MonoBehaviour {

    void Start() {
        //Gives camera random blue-ish color.
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex % 5 != 0 || UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 0)
            GetComponent<Camera>().backgroundColor = Color.HSVToRGB(Random.Range(205, 235) / 360f, 45 / 255f, 230 / 255f);
        else
            GetComponent<Camera>().backgroundColor = Color.HSVToRGB(Random.Range(350, 360) / 360f, 45 / 255f, 230 / 255f);

    }
}
