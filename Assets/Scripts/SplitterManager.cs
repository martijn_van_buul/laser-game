﻿using UnityEngine;
using System.Collections;

public class SplitterManager : MonoBehaviour {

    public int amountOfBounces;
    public bool forwardRay;

    private LineRenderer lineRenderer;

    private Ray ray;
    private RaycastHit hit;

    private ReceptorManager receptor;
    private DeflectorManager deflector;

    private Vector3 origin;

    private bool laserEnded = false;

    void Start () {
        lineRenderer = GetComponent<LineRenderer>();

        lineRenderer.startWidth = 0.1f;
        lineRenderer.endWidth = 0.1f;


        for (int i = 0; i < amountOfBounces * 2; i++)
        {
            lineRenderer.SetPosition(i, transform.position);
        }
	}

    /// <summary>
    /// Method that sends lasers if it has been hit from the right direction.
    /// </summary>
    /// <param name="origin">The place  the laser comes from.</param>
    public void ReflectLaser(Vector3 origin)
    {
        Vector3 direction = Vector3.Normalize(transform.position - origin);

        if (Vector3.SqrMagnitude(direction + transform.forward) < 0.001f)
        {
            for (int i = 0; i < amountOfBounces; i++)
            {
                shootLaser(i, determineDirection(i));

                deflector = null;

                //Increasing the charge of the receptor this laser is pointing to.
                if (receptor != null)
                {
                    receptor.increaseCharge(determineDirection(i));
                    receptor = null;
                }
            }
        }
    }

    /// <summary>
    /// Method that determines where the direction of the laser is going.
    /// </summary>
    /// <param name="laserNumber">The number of the current laser.</param>
    /// <returns>Returns a vector that determines where the laser is headed.</returns>
    private Vector3 determineDirection(int laserNumber)
    {
        switch (laserNumber)
        {
            case 0:
                if (forwardRay)
                    return -transform.forward;
                else
                    return transform.right;
            case 1:
                return transform.up;
            case 2:
                return -transform.right;
            case 3:
                return -transform.up;
            case 4:
                return -transform.forward;
            default:
                return Vector3.zero;
        }
    }

    /// <summary>
    /// Shoots a laser forwards and checks for any deflectors and/or receptors.
    /// </summary>
    private void shootLaser(int laserNumber, Vector3 direction)
    {
        //Setting up the original ray.
        origin = transform.position;
        ray = new Ray(origin, direction);

        //Bool that keeps in check whether the laser has ended.
        //It can be prolonged by deflectors.
        laserEnded = false;

        //Function that keeps sending rays until no more deflectors are found.
        while (!laserEnded)
        {
            laserEnded = true;
            //Sends a ray from the last hit location.
            if (Physics.Raycast(ray, out hit, 100, ~(1 << LayerMask.NameToLayer("LaserOnly"))))
            {
                //Setting the line towards the thing the ray hits.
                if (deflector == null)
                {
                    lineRenderer.SetPosition(1 + laserNumber * 2, hit.point + Vector3.Normalize(hit.point - origin) * hit.collider.bounds.size.z / 2);
                }
                //If the ray hits a deflector the deflector is saved and a new ray is constructed.
                //This will also make sure that another ray is cast.
                if (hit.collider.gameObject.tag == "Deflector" || hit.collider.gameObject.tag == "Player")
                {
                    //If there was already a deflector, The line of the last deflector is calculated.
                    //The origin of the last deflector is saved.
                    if (deflector != null)
                    {
                        //deflector.SetDestinationLine(hit.point + deflector.ReflectVector(origin) * hit.collider.bounds.size.z / 2);
                        origin = deflector.gameObject.transform.position;
                    }

                    //Saving the new deflector and making the new ray.
                    deflector = hit.collider.gameObject.GetComponent<DeflectorManager>();
                    ray = new Ray(hit.collider.gameObject.transform.position, deflector.ReflectVector(origin));

                    //Also makes sure the while loop continues.
                    laserEnded = false;
                }
                else if (hit.collider.gameObject.tag == "Receptor")
                {
                    //Saving the receptor to increase it's charge.
                    receptor = hit.collider.gameObject.GetComponent<ReceptorManager>();

                    //Setting the line towards the thing the ray hits.
                    //if (deflector != null)
                        //deflector.SetDestinationLine(hit.point + deflector.ReflectVector(origin) * hit.collider.bounds.size.z / 2);
                }
                else
                {
                    //Handling the ray is it hit anything else but the deflect- or receptors.
                    receptor = null;

                    //if (deflector != null)
                        //deflector.SetDestinationLine(hit.point);

                    return;
                }
            }
            else
            {
                //If the ray did not hit anything and a deflector is saved, the last deflector will have a line generated.
                if (deflector != null)
                    deflector.NoHit(origin);

                //Sets a line from this object forwards.
                else
                    lineRenderer.SetPosition(1 + laserNumber * 2, transform.position + ray.direction * 100);


                //If nothing was hit forget the receptor.
                receptor = null;
            }
        }
    }
}
