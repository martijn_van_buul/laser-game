﻿using UnityEngine;
using System.Collections;

namespace Forge3D
{
    [RequireComponent(typeof(LineRenderer))]
    public class LaserBehaviour : MonoBehaviour
    {
        public LayerMask layerMask;

        public float beamScale; // Default beam scale to be kept over distance
        public float MaxBeamLength; // Maximum beam length
        float animateUVTime;

        public bool AnimateUV; // UV Animation
        public float UVTime; // UV Animation speed

        public Transform rayImpact; // Impact transform
        public Transform rayMuzzle; // Muzzle flash transform

        LineRenderer lineRenderer; // Line rendered component
        RaycastHit hitPoint; // Raycast structure

        int frameNo; // Frame counter
        int FrameTimerID; // Frame timer reference
        float beamLength; // Current beam length
        float initialBeamOffset; // Initial UV offset 
        public float fxOffset; // Fx offset from bullet's touch point      


        void Awake()
        {
            // Get line renderer component
            lineRenderer = GetComponent<LineRenderer>();

            // Randomize uv offset
            initialBeamOffset = Random.Range(0f, 5f);
        }


        // Hit point calculation
        void Raycast()
        {
            // Prepare structure and create ray
            hitPoint = new RaycastHit();
            Ray ray = new Ray(transform.position, transform.forward);
            // Calculate default beam proportion multiplier based on default scale and maximum length
            float propMult = MaxBeamLength * (beamScale / 10f);

            // Raycast
            if (Physics.Raycast(ray, out hitPoint, MaxBeamLength, layerMask))
            {
                // Get current beam length and update line renderer accordingly
                beamLength = Vector3.Distance(transform.position, hitPoint.point);
                lineRenderer.SetPosition(1, new Vector3(0f, 0f, beamLength));

                // Calculate default beam proportion multiplier based on default scale and current length
                propMult = beamLength * (beamScale / 10f);

                // Adjust impact effect position
                if (rayImpact)
                    rayImpact.position = hitPoint.point - transform.forward * 0.5f;
            }

            // Adjust muzzle position
            if (rayMuzzle)
                rayMuzzle.position = transform.position + transform.forward * 0.1f;

            // Set beam scaling according to its length
            lineRenderer.material.SetTextureScale("_MainTex", new Vector2(propMult, 1f));
        }

        void Update()
        {
            animateUVTime += Time.deltaTime;
            animateUVTime %= 1;
            
            lineRenderer.material.SetTextureOffset("_MainTex", new Vector2(animateUVTime * UVTime + initialBeamOffset, 0f));
        }
    }
}