﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

public class LaserManager : MonoBehaviour {

    private LineRenderer lineRenderer;

    private Ray ray;
    private RaycastHit hit;

    private ReceptorManager receptor;
    private DeflectorManager deflector;
    private ParticleSystem myParticleSystem;
    private bool smokeScreenHit;
    private Vector3 origin;

    private bool laserEnded = false;

    private bool laserMovingDone = true;
    private float movementTime = 0.2f;

    private Vector3 hitDirection;
    private List<Vector3> lineRendererPositions;

    public float beamScale; // Default beam scale to be kept over distance
    private float beamLength = 0; // Current beam length
    private float goalBeamLength = 0; // Current beam length
    private float UVPlacement;

    private float laserSpeed = 35f;
    private bool laserHit = false;

    private AudioSource myAudio;
    private AudioClip myClip;

    void Start () {
        //Setting up the initial LineRenderer.
        lineRenderer = GetComponent<LineRenderer>();

        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex != 0)
        {
            lineRenderer.startWidth = 0.5f;
            lineRenderer.endWidth = 0.5f;
        }
        else
        {
            lineRenderer.startWidth = 0.86f;
            lineRenderer.endWidth = 0.86f;
        }


        lineRendererPositions = new List<Vector3>();

        myAudio = GetComponent<AudioSource>();
        myClip = (AudioClip)Resources.Load("Sounds/Reflection");
        myParticleSystem = GetComponentInChildren<ParticleSystem>();
    }

    void LateUpdate () {
        lineRendererPositions.Clear();
        lineRendererPositions.Add(transform.position);

        shootLaser();

        deflector = null;

        //Increasing the charge of the receptor this laser is pointing to.
        if (receptor != null && laserHit)
        {
            receptor.increaseCharge(hitDirection);
            receptor = null;
        }
    }

    /// <summary>
    /// Shoots a laser forwards and checks for any deflectors and/or receptors.
    /// </summary>
    private void shootLaser()
    {
        //Setting up the original ray.
        origin = transform.position;
        ray = new Ray(origin, transform.forward);
        hitDirection = Vector3.zero;

        //rayMuzzle.position = transform.position + transform.forward * 0.1f;
        

        //Bool that keeps in check whether the laser has ended.
        //It can be prolonged by deflectors.
        laserEnded = false;
        smokeScreenHit = false;

        //Function that keeps sending rays until no more deflectors are found.
        while (!laserEnded)
        {
            laserEnded = true;
            //Sends a ray from the last hit location.
            if (Physics.Raycast(ray, out hit, 50, ~(1 << LayerMask.NameToLayer("LaserOnly"))))
            {
                //If the ray hits a deflector the deflector is saved and a new ray is constructed.
                //This will also make sure that another ray is cast.
                if (hit.collider.gameObject.tag == "Deflector" || hit.collider.gameObject.tag == "Player")
                {
                    //If there was already a deflector, The line of the last deflector is calculated.
                    //The origin of the last deflector is saved.
                    if (deflector != null)
                    {
                        origin = deflector.gameObject.transform.position;
                    }

                    //Saving the new deflector
                    deflector = hit.collider.gameObject.GetComponent<DeflectorManager>();

                    if (deflector.ReflectVector(origin) == Vector3.zero)
                        lineRendererPositions.Add(hit.point);
                    else
                        lineRendererPositions.Add(hit.point + ray.direction * hit.collider.bounds.size.z / 2f);

                    //Making a new ray
                    ray = new Ray(hit.collider.gameObject.transform.position, deflector.ReflectVector(origin));

                    //Also makes sure the while loop continues.
                    laserEnded = false;

                }
                else if (hit.collider.gameObject.tag == "Receptor")
                {
                    //Saving the receptor to increase it's charge.
                    receptor = hit.collider.gameObject.GetComponent<ReceptorManager>();

                    hitDirection = ray.direction;

                    //Setting the line towards the thing the ray hits.
                    lineRendererPositions.Add(hit.point);
                }
                else if (hit.collider.gameObject.tag == "Splitter")
                    hit.collider.gameObject.GetComponent<SplitterManager>().ReflectLaser(origin);
                else if(hit.collider.gameObject.CompareTag("Smoke Screen"))
                {
                    //Handling the ray is it hit anything else but the deflect- or receptors.
                    receptor = null;
                    smokeScreenHit = true;
                    lineRendererPositions.Add(hit.point);
                }
                else
                {
                    //Handling the ray is it hit anything else but the deflect- or receptors.
                    receptor = null;

                    lineRendererPositions.Add(hit.point);
                }
            }
            else
            {
                //If the ray did not hit anything and a deflector is saved, the last deflector will have a line generated.
                if (deflector != null && Vector3.Distance(deflector.NoHit(origin), Vector3.zero) > 0.001f)
                    lineRendererPositions.Add(deflector.NoHit(origin));
                //Sets a line from this object forwards.
                else if (deflector == null)
                    lineRendererPositions.Add(transform.position + ray.direction * 35);

                //If nothing was hit forget the receptor.
                receptor = null;
            }
        }
        drawLaser(lineRendererPositions);
    }

    /// <summary>
    /// Method that the button calls to turn the laser.
    /// It also resets the LineRenderer.
    /// </summary>
    public void moveLaserFromButton()
    {
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, transform.position);
        if(laserMovingDone)
            Timing.RunCoroutine(moveLaser());
    }

    /// <summary>
    /// Moves the laser around an axis around a point.
    /// </summary>
    /// <param name="point">The point it rotates around</param>
    /// <param name="axis">The axis it rotates around.</param>
    /// <returns>Returns IEnumerator</returns>
    public IEnumerator<float> moveLaser()
    {
        laserMovingDone = false;
        float counter = 0;
        while (counter < 90)
        {
            if (!((counter + 90 / (movementTime / Time.deltaTime)) > 90))
                transform.RotateAround(transform.position, new Vector3(0, 1, 0), (90 / (movementTime / Time.deltaTime)));

            counter += (90 / (movementTime / Time.deltaTime));


            yield return Timing.WaitForOneFrame;
        }

        transform.eulerAngles = roundRotations(transform.eulerAngles);

        laserMovingDone = true;
    }

    /// <summary>
    /// Rounds down the angles towards 90 degrees.
    /// </summary>
    /// <param name="eulerAngler"> The vector that is to be rounded</param>
    /// <returns>returns rounded vector</returns>
    private Vector3 roundRotations(Vector3 eulerAngler)
    {
        Vector3 roundedVector = eulerAngler;
        roundedVector.x = Mathf.Round(roundedVector.x / 90) * 90;
        roundedVector.y = Mathf.Round(roundedVector.y / 90) * 90;
        roundedVector.z = Mathf.Round(roundedVector.z / 90) * 90;
        return roundedVector;
    }

    void drawLaser(List<Vector3> goallineRendererPositions)
    {
        List<Vector3> goallineRendererPositionsCopy = goallineRendererPositions;

        float oldGoalBeamLength = goalBeamLength;
        goalBeamLength = 0;

        for (int i = 1; i < goallineRendererPositionsCopy.Count; i++)
            goalBeamLength += Vector3.Distance(goallineRendererPositionsCopy[i - 1], goallineRendererPositionsCopy[i]);

        if (oldGoalBeamLength < goalBeamLength && goallineRendererPositionsCopy.Count > lineRenderer.positionCount && lineRenderer.positionCount > 1)
            myAudio.PlayOneShot(myClip);

        lineRenderer.positionCount = goallineRendererPositionsCopy.Count;

        if (Vector3.Distance(goallineRendererPositionsCopy[0], goallineRendererPositionsCopy[1]) < Vector3.Distance(goallineRendererPositionsCopy[0], lineRenderer.GetPosition(1)))
            beamLength = Vector3.Distance(goallineRendererPositionsCopy[0], goallineRendererPositionsCopy[1]);


        beamLength = Mathf.Clamp(beamLength + Time.deltaTime * laserSpeed, 0, goalBeamLength);
        laserHit = beamLength == goalBeamLength;

        if (laserHit && Vector3.Distance(goallineRendererPositionsCopy[goallineRendererPositionsCopy.Count - 1], goallineRendererPositionsCopy[goallineRendererPositionsCopy.Count - 2]) < 34)
        {
            if (!smokeScreenHit && Random.Range(0, 1f) < 0.15f)
            {
                var emitParams = new ParticleSystem.EmitParams();
                emitParams.position = goallineRendererPositionsCopy[goallineRendererPositionsCopy.Count - 1];

                emitParams.velocity = (-Vector3.Normalize(goallineRendererPositionsCopy[goallineRendererPositionsCopy.Count - 1] - goallineRendererPositionsCopy[goallineRendererPositionsCopy.Count - 2]) + Random.insideUnitSphere) * 1.5f;

                myParticleSystem.Emit(emitParams, 1);
            }
        }

        lineRenderer.SetPosition(0, goallineRendererPositionsCopy[0]);

        float beamUsed = 0;
        for (int i = 1; i < goallineRendererPositionsCopy.Count; i++)
        {
            if (Vector3.Distance(goallineRendererPositionsCopy[i - 1], goallineRendererPositionsCopy[i]) + beamUsed < beamLength)
            {
                lineRenderer.SetPosition(i, goallineRendererPositionsCopy[i]);
                beamUsed += Vector3.Distance(goallineRendererPositionsCopy[i - 1], goallineRendererPositionsCopy[i]);

                var emitParams = new ParticleSystem.EmitParams();
                emitParams.position = goallineRendererPositionsCopy[i];


                if(goallineRendererPositionsCopy.Count - 1 >= i + 1)
                {
                    emitParams.velocity = (Vector3.Normalize(goallineRendererPositionsCopy[i - 1] - goallineRendererPositionsCopy[i]) + Vector3.Normalize(goallineRendererPositionsCopy[i + 1] - goallineRendererPositionsCopy[i]) + Random.insideUnitSphere) * 1.5f;
                    if (Random.Range(0, 1f) < 0.20f)
                        myParticleSystem.Emit(emitParams, 1);
                }
            }
            else
            {
                if (Random.Range(0, 1f) < 0.015f)
                {
                    var emitParams = new ParticleSystem.EmitParams();
                    emitParams.position = goallineRendererPositionsCopy[i - 1] + Vector3.Normalize(goallineRendererPositionsCopy[i] - goallineRendererPositionsCopy[i - 1]) * Random.Range(0, (beamLength - beamUsed));

                    emitParams.velocity = (Vector3.Normalize(goallineRendererPositionsCopy[i] - goallineRendererPositionsCopy[i - 1]) + Random.insideUnitSphere) * 1.5f;

                    myParticleSystem.Emit(emitParams, 1);
                }


                lineRenderer.SetPosition(i, goallineRendererPositionsCopy[i - 1] + Vector3.Normalize(goallineRendererPositionsCopy[i] - goallineRendererPositionsCopy[i - 1]) * (beamLength - beamUsed));
                lineRenderer.positionCount = i + 1;

                if (Vector3.Distance(goallineRendererPositionsCopy[i - 1], goallineRendererPositionsCopy[i]) + beamUsed - laserSpeed * Time.deltaTime < beamLength && i != goallineRendererPositionsCopy.Count - 1)
                    myAudio.PlayOneShot(myClip);
                break;
            }
        }

        UVPlacement = beamLength * (beamScale / 10f);
        lineRenderer.material.SetTextureScale("_MainTex", new Vector2(UVPlacement, 1f));
    }
}
