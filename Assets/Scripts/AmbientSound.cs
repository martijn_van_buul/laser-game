﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MovementEffects;

public class AmbientSound : MonoBehaviour
{

    public static AmbientSound instance;
    private float startVolume = 0.5f;
    private static AudioSource musicAudioSource;

    void Awake()
    {
        if (instance != this)
        {
            if (instance != null)
                Destroy(gameObject);
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
                musicAudioSource = GetComponent<AudioSource>();
                Timing.RunCoroutine(instance.FadeMusic(1.5f, true));
                SceneManager.sceneLoaded += OnLevelFinishedLoading;
            }
        }
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (instance != null)
            Timing.RunCoroutine(instance.FadeMusic(1.5f, true));
    }

    public IEnumerator<float> FadeMusic(float duration, bool fadeIn)
    {
        float timer = duration;
        while (timer > 0)
        {
            if (fadeIn)
                musicAudioSource.volume = (1 - (timer / duration)) * startVolume;
            else
                musicAudioSource.volume = ((timer / duration)) * startVolume;

            timer -= Time.deltaTime;
            yield return Timing.WaitForOneFrame;
        }
    }
}
