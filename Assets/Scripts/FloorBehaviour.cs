﻿using UnityEngine;
using System.Collections;

public class FloorBehaviour : MonoBehaviour {

	public void StepOn()
    {
        foreach (Transform child in transform)
            if (child.name.Contains("BrokenPart"))
                if (Random.Range(0f, 1f) > 0.5f)
                {
                    child.gameObject.AddComponent<Rigidbody>();
                    child.parent = null;
                }
    }
}
