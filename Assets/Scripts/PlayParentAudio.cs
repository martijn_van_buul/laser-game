﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class PlayParentAudio : MonoBehaviour {

    public AudioClip myClip;

    public void PlayAudio()
    {
        GetComponentInParent<AudioSource>().PlayOneShot(myClip, 2);
        GetComponentInParent<PlayerMovement>().EmitParticles();
        Timing.RunCoroutine(CameraMovement.instance.Shake(0.2f, 1.6f));
        Timing.RunCoroutine(MoveBlocks.instance.DisperseAtStart(transform.parent));
     }

}
