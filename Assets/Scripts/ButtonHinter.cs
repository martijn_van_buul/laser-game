﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHinter : MonoBehaviour {

    public GameObject FadingObject;
    private Animator fadingAnimator;
    public List<Vector3> hintPositions;
    private GameObject player;
    private bool isHinting;
    private bool hinted = false;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        fadingAnimator = FadingObject.GetComponent<Animator>();
        GameManager.instance.playerRespawned += UpdatePlayer;
    }

    void UpdatePlayer()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        hinted = true;
    }


    // Update is called once per frame
    void Update () {
        isHinting = false;
        if (!hinted)
        {
            foreach (Vector3 hintPosition in hintPositions)
                if (Vector3.Distance(player.transform.position, hintPosition) < 0.5f)
                    isHinting = true;

        }
        fadingAnimator.SetBool("IsHinting", isHinting);

    }
}
