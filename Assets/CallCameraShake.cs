﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallCameraShake : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AddCameraShake()
    {
        GetComponentInChildren<CameraShake>().AddTrauma(0.5f);
    }
}
