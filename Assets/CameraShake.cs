﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

    public static CameraShake instance;

    [SerializeField]
    private float maxAngle = 12;
    [SerializeField]
    private float maxOffset = 1.2f;
    [SerializeField]
    private float traumaReduction = 1.5f;
    [SerializeField]
    private float perlinAccelerator = 10f;

    private float shake = 0;
    private float trauma = 0;

    int seed1; 
    int seed2;
    int seed3;

    Vector3 cameraOffset;

    private void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        cameraOffset = transform.localPosition;

        seed1 = Random.Range(int.MinValue, int.MaxValue);
        seed2 = Random.Range(int.MinValue, int.MaxValue);
        seed3 = Random.Range(int.MinValue, int.MaxValue);
    }

    // Update is called once per frame
    void FixedUpdate () {
        trauma = Mathf.Clamp01(trauma - traumaReduction * Time.fixedDeltaTime);
        shake = Mathf.Clamp01(Mathf.Pow(trauma, 2f));

        float offsetX = maxOffset * shake * (1 - 2 * Mathf.PerlinNoise(seed1, perlinAccelerator * Time.timeSinceLevelLoad));
        float offsetY = maxOffset * shake * (1 - 2 * Mathf.PerlinNoise(seed2, perlinAccelerator * Time.timeSinceLevelLoad));

        transform.eulerAngles = maxAngle * Vector3.forward * shake * (1 - 2 * Mathf.PerlinNoise(seed3, perlinAccelerator * Time.timeSinceLevelLoad));
        transform.localPosition = cameraOffset + new Vector3(offsetX, offsetY, 0);
    }

    /// <summary>
    /// Method for adding trauma to the camera shake.
    /// </summary>
    /// <param name="addedTrauma">The trauma that will be added.</param>
    public void AddTrauma(float addedTrauma)
    {
        addedTrauma = (addedTrauma * (1 - trauma));
        trauma = Mathf.Clamp(trauma + addedTrauma, 0, 1);
    }
}
