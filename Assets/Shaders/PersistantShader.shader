﻿Shader "Persistant"
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Texture", 2D) = "white" 
	}

	Category
	{
		SubShader
		{
			Tags{ "Queue" = "Overlay+1" }

			Pass
			{
				ZWrite On
				ZTest Less
				Lighting Off
				Color[_Color]
			}
			Pass
			{
				ZWrite On
				ZTest Less
				Color[_Color]
			}
		}
	}

	FallBack "Specular", 1
}